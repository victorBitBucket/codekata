input = File.read(ARGV[0])
hash = Hash.new()
words = input.split(' ')
i = 0
maxLength = words.length
while i < maxLength - 2 do
    bigram = words[i] + ' ' + words[i + 1] 
    if !hash.has_key?(bigram)
        hash[bigram] = [words[i + 2]]
    else
        hash[bigram].push(words[i + 2])
    end 
    i += 1
end

nextKey = hash.keys[rand(hash.keys.length)]
finalText = nextKey

while hash.has_key?(nextKey) do
  wordArrayForNextWord = hash[nextKey]
  thisWord = wordArrayForNextWord[rand(wordArrayForNextWord.length)]
  nextKey = nextKey.split(' ')[1] + ' ' + thisWord
  finalText += ' ' + thisWord
end

puts(finalText)