require 'sinatra'
require 'json'
require_relative 'anagrams'

class WebApp < Sinatra::Base
  
  configure do
    @@anagramObject = Anagrams.new()
  end

  get '/:word' do

    word = params['word']

    result = @@anagramObject.findWord(word)
    if (result != nil)
      content_type :json
      puts result.to_json
      result.to_json
    else
      "'#{word}' not found"
    end
  
  end

  post '/words' do
  
    request.body.rewind  # in case someone already read it
    data = JSON.parse request.body.read
    @@anagramObject.process(data)
 
    "Processed " + data.count.to_s + " words"

  end
  
  run! if app_file == $0
  
end