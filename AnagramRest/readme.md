In the POST you send the word array json. The service finds all of the anagrams and stores the hash in memory. From that point you can GET with a word, and it will return all anagrams found in the original list array.

start the service on localhost:
> ruby anagramRest.rb

run the test
> ruby run.rb words.txt