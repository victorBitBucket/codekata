require 'net/http'
require 'json'

wordsArray = File.read(ARGV[0]).split("\n")

uri = URI('http://localhost:4567/words')
request = Net::HTTP::Post.new(uri)
request.content_type = 'application/json'
request.body = JSON.generate wordsArray

response = Net::HTTP.start(uri.hostname, uri.port) do |http|
  http.request(request)
end

puts "\nPOST response: " + response.body + "\n"

puts "GET response for 'parse': " + Net::HTTP.get_response(URI('http://localhost:4567/parse')).body + "\n"
puts "GET response for 'god': " + Net::HTTP.get_response(URI('http://localhost:4567/god')).body + "\n"
puts "GET response for 'n3wb': " + Net::HTTP.get_response(URI('http://localhost:4567/n3wb')).body + "\n\n"