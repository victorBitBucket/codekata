@interface VGEngine : NSObject
{
    NSDictionary *_digitPaths;
}

@property NSArray *startingNumbers;
@property NSMutableArray *phoneNumberList;

@end

@implementation VGEngine

- (id)init
{
    self = [super init];
    _phoneNumberList = [[NSMutableArray alloc] init];
    _digitPaths = @{@"1":@[@"6", @"8"],
                         @"2":@[@"7", @"9"],
                         @"3":@[@"4", @"8"],
                         @"4":@[@"0", @"3", @"9"],
                         @"5":@[],
                         @"6":@[@"0", @"1", @"7"],
                         @"7":@[@"2", @"6"],
                         @"8":@[@"1", @"3"],
                         @"9":@[@"2", @"4"],
                         @"0":@[@"4", @"6"]
                         };
    
    return self;
}

- (void)start
{
    for (NSString *startingNumber in _digitPaths.allKeys)
    {
        // skip area codes starting with 0 or 1
        if (![startingNumber isEqualToString:@"0"] && ![startingNumber isEqualToString:@"1"])
            [self digWithDigit:startingNumber depth:10 currentNumber:@""];
    }

    NSLog (@"Answer: %li", _phoneNumberList.count);
    NSLog (@"Check dat work: %@", _phoneNumberList);
}

- (void)digWithDigit:(NSString*)digit depth:(int)depth currentNumber:(NSString*)currentNumber
{
    NSString *appendedNumber = [currentNumber stringByAppendingString:digit];

    if (depth == 1)
    {
        [_phoneNumberList addObject:appendedNumber];
        return;
    }
    else
    {
        NSArray *possibleValues = _digitPaths[digit];
        for (NSString *nextValue in possibleValues)
        {
            [self digWithDigit:nextValue depth:depth - 1 currentNumber:appendedNumber];
        }
    }
}

@end