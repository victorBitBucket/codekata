wordsArray = File.read(ARGV[0]).split("\n")

anagramHash = Hash.new()

wordsArray.each() do |word|

  sortedLetters = word.chars.sort.join.strip #trims the whitespace
  if (word.index(" "))
    word = "\"" + word + "\""
  end
  if (anagramHash.has_key?(sortedLetters))
    anagramHash[sortedLetters] += " " + word
  else
    anagramHash[sortedLetters] = word
  end

end

File.write(ARGV[1], anagramHash.values.join("\n"))