require 'minitest/autorun'
require_relative 'anagrams'

describe "Anagrams" do
  
  before do
    @anagrams = Anagrams.new
  end
  
  it "algorithm works properly for one group" do
    input = ["evil", "vile", "live"]
    result = Hash["eilv" => "evil vile live"]    
    @anagrams.process(input).must_equal result
  end

  it "algorithm works properly for bigger group" do
    input = ["earl","lives","sword","dog","tac","agar man","atc","words","elvis","anagram","cat","a nag arm"]
    result = Hash[{"aelr"=>"earl", "eilsv"=>"lives elvis", "dorsw"=>"sword words", "dgo"=>"dog", "act"=>"tac atc cat", "aaagmnr"=>"\"agar man\" anagram \"a nag arm\""}]
    @anagrams.process(input).must_equal result
  end
  
  it "raises exception if file input is invalid" do
    proc{@anagrams.runFile("badfile.txt", "")}.must_raise Errno::ENOENT
  end
  
  it "matches the output expected for words.txt regardless or word order" do
    @anagrams.runFile("words.txt", "output.txt")

    newArrayList = Array.new
    oldArrayList = Array.new
    
    File.read("output.txt").split("\n").sort.each() do |line|
      newArrayList.push(line.split(" ").sort)
    end
    
    File.read("expectedWords.txt").split("\n").sort.each() do |line|
      oldArrayList.push(line.split(" ").sort)
    end
    
    oldArrayList.must_equal newArrayList
  end
  
end