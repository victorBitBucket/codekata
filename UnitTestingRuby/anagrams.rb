class Anagrams

  def runFile (inputFile, outputFile)
    wordsArray = self.readFromFile(inputFile)
    hash = self.process(wordsArray)
    self.printToFile(outputFile, hash)
  end
  
  def readFromFile(inputFile)
    return File.read(inputFile).split("\n")  
  end
  
  def process(wordsArray)    
    anagramHash = Hash.new()
    
    wordsArray.each() do |word|
     sortedLetters = word.chars.sort.join.strip #trims the whitespace
      if (word.index(" "))
        word = "\"" + word + "\""
      end
      if (anagramHash.has_key?(sortedLetters))
        anagramHash[sortedLetters] += " " + word
      else
        anagramHash[sortedLetters] = word
      end
    end
    
    return anagramHash
  end

  def printToFile (outputFile, hash)
    File.write(outputFile, hash.values.join("\n"))
  end
  
end