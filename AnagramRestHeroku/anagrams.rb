class Anagrams

  def anagramHash
    if (@anagramHash == nil)
      @anagramHash = Hash.new()
    end
    return @anagramHash
  end

  def runFile (inputFile, outputFile)
    wordsArray = self.readFromFile(inputFile)
    hash = self.process(wordsArray)
    self.printToFile(outputFile, hash)
  end
  
  def readFromFile(inputFile)
    return File.read(inputFile).split("\n")  
  end
  
  def process(wordsArray)    
    
    @anagramHash = Hash.new()
    
    wordsArray.each() do |word|
     sortedLetters = word.chars.sort.join.strip #trims the whitespace
      if (word.index(" "))
        word = "\"" + word + "\""
      end
      if (anagramHash().has_key?(sortedLetters))
        anagramHash()[sortedLetters] += " " + word
      else
        anagramHash()[sortedLetters] = word
      end
    end
  end

  def printToFile (outputFile, hash)
    File.write(outputFile, hash.values.join("\n"))
  end
  
  def findWord (word)
    sortedLetters = word.chars.sort.join.strip
    if (anagramHash().has_key?(sortedLetters))
      return anagramHash()[sortedLetters]
    else
      return nil
    end
     
  end
  
end