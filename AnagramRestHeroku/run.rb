require 'net/http'
require 'json'

wordsArray = File.read(ARGV[0]).split("\n")

http = Net::HTTP.new('anagramrestservice.herokuapp.com')

puts "\nPOST response: " + http.send_request('POST', '/words', JSON.generate(wordsArray), header = {'content-type' => 'application/json'}).body + "\n"
puts "GET response for 'parse': " + http.send_request('GET', '/parse').body + "\n"
puts "GET response for 'god': " + http.send_request('GET', '/god').body + "\n"
puts "GET response for 'n3wb': " + http.send_request('GET', '/n3wb').body + "\n\n"