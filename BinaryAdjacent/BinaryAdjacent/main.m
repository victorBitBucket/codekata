//
//  main.m
//  BinaryAdjacent
//
//  Created by Germanis, Victor (Contractor) on 4/23/16.
//  Copyright © 2016 Germanis, Victor (Contractor). All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        if (argc > 1)
        {
            int length = atoi(argv[1]);
            
            if (length >= 0 && length != NSNotFound)
            {
                long maxNumber = pow(2, length);
                long binsWithout11 = 0;
                
                for (long currentNumber = 0; currentNumber < maxNumber; currentNumber++)
                {
                    long whittle = currentNumber;
                    int lastBin = 0;
                    bool has11 = false;
                    
                    for (int i = length - 1; i >= 0; i--)
                    {
                        long maxValue = pow(2, i);
                        if (whittle >= maxValue)
                        {
                            if (lastBin == 1)
                            {
                                has11 = true;
                                break;
                            }
                            
                            lastBin = 1;
                            whittle -= maxValue;
                        }
                        else
                        {
                            lastBin = 0;
                        }
                    }
                    
                    binsWithout11 += !has11;
                }
            
                printf("Length: %i\nBinary numbers without consecutive 1's: %li\n\n", length, binsWithout11);
            }
        }
    }
    
    return 0;
}